<!DOCTYPE html>
<html lang="en">
<head>
<title>juego</title>
<style>
td
{
    height: 100px;
    width: 100px;
    border: solid 3px black;
    text-align:center;
    font-size:60pt;
    font-family: 'comfortaa',cursive;
}
td:flotar
{
    background-color: #dbdbdb;
    cursor:pointer;
    width: 180px;
}
</style>
<script type="text/javascript">
var ganador = new Array();
var jugador1selec = new Array();
var jugador2selec  = new Array();
var numjugador  = 2;
var cantidad = 0;
var movi = 0;
var puntos1 = 0;    
var puntos2 = 0;    
var empate=0;
var size = 3;
var l=0;

function grafi() {
    var Raya = document.getElementById("game");
    var conteo = 1;
    
    while (Raya.hasChildNodes()) {
        Raya.removeChild(Raya.firstChild);
    }

    for (s = 0; s < 3; s++) {
        var fila = document.createElement("tr");
        
        for (r = 0; r < 3; r++) {
            var col = document.createElement("td");
            col.id = conteo;
            col.innerHTML = conteo;

            var jugadas = function(e) {
                if (cantidad == 0) {
                    this.innerHTML = "X";
                    jugador1selec .push(parseInt(this.id));
                    l++;
                    jugador1selec .sort(function(a, b) { return a - b });
                }
                else {
                    this.innerHTML = "O";
                    jugador2selec .push(parseInt(this.id));
                    l++;
                    jugador2selec .sort(function(a, b) { return a - b });
                }
                movi++;
                var gana= verificar();

                if (gana)
                {
                    if(cantidad == 0)
                    puntos1++;
                    else
                     puntos2++;
                     document.getElementById("jugador1").innerHTML = puntos1;
                     document.getElementById("jugador2").innerHTML = puntos2;
                     document.getElementById("game1").value = puntos1;
                     document.getElementById("game2").value = puntos2;
                     l=0;
                     reset();
                     grafi();
                }
                else if(l==9){
                    empate++;
                    l=0;
                    document.getElementById("empate").innerHTML = empate;
                    document.getElementById("empatar").value = empate;
                    reset();
                     grafi();
                }
                else
                {                 
                    if (cantidad == 0)
                    cantidad = 1;
                    else
                        cantidad= 0;
                    this.removeEventListener('click', arguments.callee);                    
                }
            };
            col.addEventListener('click', jugadas);
            fila.appendChild(col);
            conteo++;
        }
        Raya.appendChild(fila);
    }
    loadAnswers();
}
function reset()
{
    jugActual = 0;
    jugador1selec  = new Array();
    jugador2selec = new Array();
}
function loadAnswers()
{
    ganador.push([1, 2, 3]);
    ganador.push([4, 5, 6]);
    ganador.push([7, 8, 9]);
    ganador.push([1, 4, 7]);
    ganador.push([2, 5, 8]);
    ganador.push([3, 6, 9]);
    ganador.push([1, 5, 9]);
    ganador.push([3, 5, 7]);
}
function verificar() {
    var win = false;
    var playerSelections = new Array();
    if (cantidad == 0)
        playerSelections = jugador1selec ;
    else
	playerSelections = jugador2selec ;    
    if (playerSelections.length >= size) { 
        for (i = 0; i < ganador.length; i++) {
            var sets = ganador[i]; 
            var setFound = true;            
            for (r = 0; r < sets.length; r++) {
                var found = false;
                for (s = 0; s < playerSelections.length; s++) {
                    if (sets[r] == playerSelections[s]) {
                        found = true;
                        break;
                    }
                }
                if (found == false) {
                    setFound = false;
                    break;
                }
            }
            if (setFound == true) {
                win = true;
                break;
            }
        }
    }
    return win;
} 
window.onload = grafi;
</script>
</head>
<body style="background-color:olive">
<center><h1 style="text-align:center">III EN RAYA </h1> 
<table>
    <tr>
        <div style="text-align:center;margin:0 auto;width:50%;padding-top:20px;"></div>
        <div style="float:left;">Player 1</div>
        <div style="font-size:30pt;" id="jugador1">0</div>
        <div style="float:left;margin-left:20px;">Player 2</div>
        <div id="jugador2" style="font-size:30pt;">0</div>
        <div style="float:left;margin-left:20px;">Empate</div>
        <div id="empate" style="font-size:30pt;">0</div></div>
    </tr>
    <tr >
        <div style="text-aling:center"><table id="game" style="float:left;margin-left:20px;"></table></div>
        <div class="clear"></div>
    </tr>
</table></center>
<tr><form action="imprimir.php" method="POST">
<input type="text" id="game1" name="game1" value="0" style="visibility:hidden">
<input type="text" id="game2" name="game2" value="0"style="visibility:hidden">
<input type="text" id="empatar" name="empatar" value="0"style="visibility:hidden" >
<button type="submit">imprimir</button></tr>
</form>
</body>
</html>
